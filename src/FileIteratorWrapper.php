<?php

namespace Drupal\migrate_source_yaml_fileset;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Wraps the Symfony Finder iterator.
 */
class FileIteratorWrapper implements \Iterator {

  /**
   * The iterator.
   *
   * @var \Iterator
   */
  protected $it;

  /**
   * The base path of the directory.
   *
   * @var string
   */
  protected $basePath;

  /**
   * Constructs a FileIteratorWrapper.
   *
   * @param \Iterator $it
   *   The iterator.
   * @param string $basePath
   *
   */
  public function __construct(\Iterator $it, $basePath) {
    $this->it = $it;
    $this->basePath = $basePath;
  }

  /**
   * {@inheritdoc}
   */
  public function current() {
    /** @var \Symfony\Component\Finder\SplFileInfo $current */
    $current = $this->it->current();

    try {
      $data = Yaml::parse(file_get_contents($current->getPathname()));
    }
    catch (ParseException $exception) {
      $data = [];
    }

    $data += [
      'filename' => $current->getFilename(),
      'pathname' => $current->getPathname(),
    ];

    $data['id'] = substr($current->getPathname(), strlen($this->basePath));
    $data['id'] = ltrim($data['id'], DIRECTORY_SEPARATOR);

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function next() {
    $this->it->next();
  }

  /**
   * {@inheritdoc}
   */
  public function key() {
    return $this->it->key();
  }

  /**
   * {@inheritdoc}
   */
  public function valid() {
    return $this->it->valid();
  }

  /**
   * {@inheritdoc}
   */
  public function rewind() {
    $this->it->rewind();
  }

}
