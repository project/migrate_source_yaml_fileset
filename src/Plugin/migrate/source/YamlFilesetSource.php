<?php

namespace Drupal\migrate_source_yaml_fileset\Plugin\migrate\source;

use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_source_yaml_fileset\FileIteratorWrapper;
use Symfony\Component\Finder\Finder;

/**
 * YAML file set source.
 *
 * @MigrateSource(
 *   id = "yaml_fileset"
 * )
 */
class YamlFilesetSource extends SourcePluginBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    // Path is required.
    if (empty($this->configuration['path'])) {
      throw new MigrateException('You must declare the "file" to the source Yaml file in your source settings.');
    }

    // ID field(s) are required.
    if (empty($this->configuration['ids'])) {
      throw new MigrateException('You must declare "ids" as a unique array of fields in your source settings.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return 'YAML encoded file set.';
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return $this->configuration['ids'];
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $it = (new Finder())->in($this->configuration['path'])
      ->name('*.{yml,yaml}')
      ->getIterator();
    return new FileIteratorWrapper($it, $this->configuration['path']);
  }

}
